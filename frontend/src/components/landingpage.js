import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';


class Landing extends Component {
  state = {
    todos: []
  };

  async componentDidMount() {
    try {
      const res = await fetch('http://127.0.0.1:8000/api/'); // fetching the data from api, before the page loaded
      const todos = await res.json();
      this.setState({
        todos
      });
    } catch (e) {
      console.log(e);
    }
  }
  render() {
    return(
      <div style={{width: '100%', margin: 'auto'}}>
        <Grid className="landing-grid">
          <Cell col={12}>
            
            <img
              src="https://scontent-sin2-2.xx.fbcdn.net/v/t1.0-9/29250146_1088464427963129_296609524831678953_n.jpg?_nc_cat=111&_nc_oc=AQkn7Nc8-C03AhHcnwDSTHjsae3EBtqkchwzamnv6q33Cql2B5dUUY4GUPlAc0qURXo&_nc_ht=scontent-sin2-2.xx&oh=2564354d65c28549268c32ba076368f6&oe=5EBECD2A"
              alt="avatar"
              className="avatar-img"
              />

            <div className="banner-text">
            {this.state.todos.map(item => (
 <          div key={item.id}>
            <h1>{item.title}</h1>
            <span><h1>{item.description}</h1></span>
          </div>
               ))}
            <hr/>

          <p>HTML/CSS | Bootstrap | JavaScript | React | React Native | NodeJS | Express | MongoDB</p>

        <div className="social-links">

          {/* LinkedIn */}
          <a href="http://google.com" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-linkedin-square" aria-hidden="true" />
          </a>

          {/* Github */}
          <a href="http://google.com" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-github-square" aria-hidden="true" />
          </a>

          {/* Freecodecamp */}
          <a href="http://google.com" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-free-code-camp" aria-hidden="true" />
          </a>

          {/* Youtube */}
          <a href="http://google.com" rel="noopener noreferrer" target="_blank">
            <i className="fa fa-youtube-square" aria-hidden="true" />
          </a>

        </div>
            </div>
          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Landing;
